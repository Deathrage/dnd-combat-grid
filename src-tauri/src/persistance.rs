use std::{ffi::OsString, path::PathBuf};

use tauri::{api::{dialog::blocking::{confirm, FileDialogBuilder}, path::document_dir}, AppHandle, Window};

use crate::{combat::NEW_COMBAT_TITLE, main_window::{get_title_prefix, update_title}};

#[derive(Clone)]
pub struct CurrentFile {
    pub pending_changes: bool,
    pub file_location: Option<PathBuf>,
}

static mut CURRENT_FILE: CurrentFile = CurrentFile {
    pending_changes: false,
    file_location: None
};

pub fn get_current_file_location() -> Option<PathBuf> {
    unsafe {
        CURRENT_FILE.clone().file_location
    }
}

pub fn set_current_file(app_handle: &AppHandle, path: &PathBuf) -> Result<(), String> {
    unsafe {
        CURRENT_FILE.pending_changes = false;
        CURRENT_FILE.file_location = Some(path.to_owned());
    }

    return update_title_from_current_file(app_handle);
}

pub fn reset_current_file(app_handle: &AppHandle) -> Result<(), String> {    
    unsafe {
        CURRENT_FILE.pending_changes = false;
        CURRENT_FILE.file_location = None;
    }

    return update_title_from_current_file(app_handle);
}

pub fn mark_change(app_handle: &AppHandle) -> Result<(), String> {   
    unsafe {
        CURRENT_FILE.pending_changes = true;
    }

    return update_title_from_current_file(app_handle);
}

fn update_title_from_current_file(app_handle: &AppHandle) -> Result<(), String> {
    let current_file: &CurrentFile;

    unsafe {
        current_file = &CURRENT_FILE;
    }

    let mut title: OsString = match &current_file.file_location {
        Some(i) => i.file_name().expect("Path did not contain filename.").to_os_string(),
        None => OsString::from(NEW_COMBAT_TITLE)
    };

    if current_file.pending_changes == true {
        title.push("*");
    }  

    return update_title(app_handle, title.to_str());
}

pub fn create_json_dialog_builder() -> FileDialogBuilder {
    let mut builder = FileDialogBuilder::new()
        .add_filter("DnD Combat Grid State File", &["gsf"])
        .set_file_name("DnD Combat.gsf");

    match document_dir() {
        Some(i) => builder = builder.set_directory(i),
        None => ()
    };

    return builder;
}

pub fn confirm_save() -> bool {   
    let current_file: &CurrentFile;

    unsafe {
        current_file = &CURRENT_FILE;
    }

    if current_file.pending_changes == true {
        return confirm(None::<&Window>, get_title_prefix().as_str(), "Unsaved changes will be lost.")
    }

    return true;
}