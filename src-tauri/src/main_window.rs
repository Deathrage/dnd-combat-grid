use tauri::{AppHandle, Manager, Window};

static mut MAIN_TITLE: Option<String> = None;

pub fn set_title_prefix(main_window: &Window) {
    unsafe {
        MAIN_TITLE = Some(main_window.title().expect("Main window has no title!"));
    }
}

pub fn get_title_prefix() -> String {
    unsafe {
        MAIN_TITLE.clone().expect("No main title.")
    }
}

pub fn get_main_window(app_handle: &AppHandle) -> Result<Window, String> {
    return match app_handle.get_window("main") {
        Some(i) => Ok(i),
        None => Err("Could not find the main window.".into())
    }
}

pub fn update_title(app_handle: &AppHandle, second_part: Option<&str>) -> Result<(), String>  {
    let main_window = get_main_window(&app_handle)?;

    let mut new_title = get_title_prefix();

    if second_part.is_some() {
        new_title.push_str(" - ");
        new_title.push_str(second_part.unwrap());
    }


    match main_window.set_title(&new_title) {
        Ok(_i) => (),
        Err(_e) => return Err("Could not set title of the main window!".into())
    };

    return Ok(());
}