// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::fs::{read_to_string, write};

use combat::NEW_COMBAT_TITLE;
use main_window::{get_main_window, set_title_prefix, update_title};
use persistance::{confirm_save, create_json_dialog_builder, get_current_file_location, reset_current_file, set_current_file};
use tauri::WindowEvent;

mod persistance;
mod main_window;
mod combat;
 
#[tauri::command]
fn new(app_handle: tauri::AppHandle) -> Result<(), String> {
    reset_current_file(&app_handle)
}

#[tauri::command]
fn open(app_handle: tauri::AppHandle) -> Result<String, String> {

    let path = match create_json_dialog_builder().pick_file() {
        Some(i) => i,
        None => return Ok("".into())
    };

    set_current_file(&app_handle, &path)?;

    let data = match read_to_string(path) {
        Ok(i) => i,
        Err(_e) => return Err("Could not open file.".into())
    };

    return Ok(data.to_string())
}

#[tauri::command]
fn save_as(app_handle: tauri::AppHandle, data: &str) -> Result<(), String> {
    let path = match create_json_dialog_builder().save_file() {
        Some(i) => i,
        None => return Ok(())
    };

    set_current_file(&app_handle, &path)?;
    
    match write(path, data.as_bytes()) {
        Ok(i) => i,
        Err(_e) => return Err("Could not save to file.".into())
    };

    return Ok(());
}

#[tauri::command]
fn save(app_handle: tauri::AppHandle, data: &str) -> Result<(), String> {
    let current_file = get_current_file_location();

    let path = match current_file {
        Some(i) => i,
        None => return save_as(app_handle, data)
    };  

    match write(path, data.as_bytes()) {
        Ok(i) => i,
        Err(_e) => return Err("Could not save to file.".into())
    };

    return Ok(());
}

#[tauri::command]
fn mark_change(app_handle: tauri::AppHandle) -> Result<(), String> {
    persistance::mark_change(&app_handle)
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![new, open, save_as, save, mark_change])
        .setup(|app| {
            let app_handle = app.handle();
            let main_window = get_main_window(&app_handle).unwrap();
            
            set_title_prefix(&main_window);
            update_title(&app_handle, Some(NEW_COMBAT_TITLE)).unwrap();

            // Postpone windows closure if pending changes
            main_window.on_window_event(|event| match event {
                WindowEvent::CloseRequested { api, .. } => {
                    if confirm_save() == false {
                        api.prevent_close();
                    }
                }
                _ => {}
              });

            return Ok(());
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}




