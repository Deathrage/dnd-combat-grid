import {
  Button,
  Dialog,
  DialogActions,
  DialogBody,
  DialogSurface,
  DialogTitle,
  DialogTrigger,
  ToolbarButton,
  makeStyles,
} from '@fluentui/react-components';
import { FC } from 'react';
import { interactivity } from '../stores/interactivity';
import { RxTrash } from 'react-icons/rx';
import { gridState } from '../stores/gridState';
import { runInAction } from 'mobx';

const useClasses = makeStyles({
  wipeModal: {
    width: '18rem',
  },
});

export const WipeButton: FC = () => {
  const classes = useClasses();

  return (
    <Dialog
      modalType="non-modal"
      onOpenChange={(_, data) =>
        runInAction(() => (interactivity.paused = data.open))
      }
    >
      <DialogTrigger action="open">
        <ToolbarButton icon={<RxTrash />} />
      </DialogTrigger>
      <DialogSurface className={classes.wipeModal}>
        <DialogBody onMouseDown={e => e.stopPropagation()}>
          <DialogTitle action={<></>}>Wipe entire board clean?</DialogTitle>
          <DialogActions>
            <DialogTrigger action="close">
              <Button appearance="secondary" onClick={() => gridState.clear()}>
                Ok
              </Button>
            </DialogTrigger>
            <DialogTrigger action="close">
              <Button appearance="primary">Cancel</Button>
            </DialogTrigger>
          </DialogActions>
        </DialogBody>
      </DialogSurface>
    </Dialog>
  );
};
