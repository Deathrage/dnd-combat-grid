import {
  Button,
  Dialog,
  DialogBody,
  DialogSurface,
  DialogTrigger,
  ToolbarButton,
  makeStyles,
} from '@fluentui/react-components';
import { FC } from 'react';
import { interactivity } from '../stores/interactivity';
import { BiCog } from 'react-icons/bi';
import {
  persistStatesToJson,
  rehydrateStateFromJson,
  wipeState,
} from '../stores/persistance';
import { invoke_new, invoke_open, invoke_save, invoke_saveAs } from '../BE';
import { runInAction } from 'mobx';

const useClasses = makeStyles({
  actionMenuModal: {
    width: '28.75rem',
  },
  actionMenuGrid: {
    display: 'flex',
    flexWrap: 'wrap',
    rowGap: '2rem',
    justifyContent: 'flex-end',
  },
});

export const ActionsButton: FC = () => {
  const classes = useClasses();

  return (
    <Dialog
      modalType="non-modal"
      onOpenChange={(_, data) =>
        runInAction(() => (interactivity.paused = data.open))
      }
    >
      <DialogTrigger action="open">
        <ToolbarButton icon={<BiCog />} />
      </DialogTrigger>
      <DialogSurface className={classes.actionMenuModal}>
        <DialogBody
          onMouseDown={e => e.stopPropagation()}
          className={classes.actionMenuGrid}
        >
          <DialogTrigger action="close">
            <Button
              appearance="secondary"
              onClick={() => {
                wipeState();
                invoke_new();
              }}
            >
              New
            </Button>
          </DialogTrigger>
          <DialogTrigger action="close">
            <Button
              appearance="secondary"
              onClick={async () => {
                const data = await invoke_open();
                if (data) rehydrateStateFromJson(data);
              }}
            >
              Open
            </Button>
          </DialogTrigger>
          <DialogTrigger action="close">
            <Button
              appearance="secondary"
              onClick={() => {
                const data = persistStatesToJson();
                invoke_saveAs(data);
              }}
            >
              Save As
            </Button>
          </DialogTrigger>
          <DialogTrigger action="close">
            <Button
              appearance="secondary"
              onClick={() => {
                const data = persistStatesToJson();
                invoke_save(data);
              }}
            >
              Save
            </Button>
          </DialogTrigger>
          <DialogTrigger action="close">
            <Button appearance="primary">Cancel</Button>
          </DialogTrigger>
        </DialogBody>
      </DialogSurface>
    </Dialog>
  );
};
