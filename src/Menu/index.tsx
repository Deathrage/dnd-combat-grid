import {
  Divider,
  Toolbar,
  ToolbarButton,
  makeStyles,
  shorthands,
  tokens,
} from '@fluentui/react-components';
import { FC } from 'react';
import Draggable from 'react-draggable';
import { RxEraser } from 'react-icons/rx';
import { RxCursorArrow } from 'react-icons/rx';
import { ColorPicker } from '../ColorPicker';
import { Observer } from 'mobx-react-lite';
import { InteractionType, interactivity } from '../stores/interactivity';
import { ActorsMenu } from './ActorsMenu';
import { ActionsButton } from './ActionsButton';
import { WipeButton } from './WipeButton';

const useClasses = makeStyles({
  menu: {
    position: 'fixed',
    top: 0,
    left: 0,
    backgroundColor: tokens.colorNeutralBackground1,
    boxShadow: `${tokens.shadow16}`,
    ...shorthands.padding('4px', '8px'),
    display: 'inline-flex',
    alignItems: 'center',
  },
  menuContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  grippy: {
    cursor: 'move',
    marginLeft: '8px',
    userSelect: 'none',
    width: '20px',
    height: '40px',
    display: 'inline-block',
    lineHeight: '8px',
    verticalAlign: 'middle',
    fontSize: '12px',
    fontFamily: 'sans-serif',
    letterSpacing: '2px',
    color: '#cccccc',
    textShadow: '1px 0 1px black',
    ...shorthands.overflow('hidden'),
  },
  divider: {
    height: '100%',
    paddingTop: '.5rem',
    paddingBottom: '.5rem',
  },
});

export const Menu: FC = () => {
  const classes = useClasses();

  return (
    <Draggable
      onStart={() => {
        interactivity.paused = true;
      }}
      onStop={() => {
        interactivity.paused = false;
      }}
      cancel="#menu"
    >
      <div className={classes.menu}>
        <div className={classes.menuContainer} id="menu">
          <ColorPicker />
          <Toolbar vertical>
            <Observer>
              {() => (
                <ToolbarButton
                  icon={<RxEraser />}
                  onClick={() => interactivity.switchToEraser()}
                  appearance={
                    interactivity.interaction?.type === InteractionType.ERASER
                      ? 'primary'
                      : 'subtle'
                  }
                />
              )}
            </Observer>
          </Toolbar>
          <Divider vertical className={classes.divider} />
          <ActorsMenu />
          <Divider vertical className={classes.divider} />
          <Toolbar>
            <Observer>
              {() => (
                <ToolbarButton
                  icon={<RxCursorArrow />}
                  onClick={() => interactivity.switchToPointer()}
                  appearance={
                    interactivity.interaction?.type === InteractionType.POINTER
                      ? 'primary'
                      : 'subtle'
                  }
                />
              )}
            </Observer>
            <WipeButton />
            <ActionsButton />
          </Toolbar>
        </div>
        <div className={classes.grippy}>.. .. .. ..</div>
      </div>
    </Draggable>
  );
};
