import {
  Button,
  Dialog,
  DialogActions,
  DialogBody,
  DialogContent,
  DialogSurface,
  DialogTitle,
  DialogTrigger,
  Input,
  Label,
  Toolbar,
  ToolbarButton,
  makeStyles,
  mergeClasses,
  shorthands,
} from '@fluentui/react-components';
import { FC, useState } from 'react';
import { RxCrossCircled } from 'react-icons/rx';
import { RxPlusCircled } from 'react-icons/rx';
import { VscSave } from 'react-icons/vsc';
import { actors } from '../stores/actors';
import { Observer } from 'mobx-react-lite';
import { InteractionType, interactivity } from '../stores/interactivity';
import { startActorDrag, getDraggedActorName } from '../Grid/drag';
import { gridState } from '../stores/gridState';

const useStyles = makeStyles({
  dialog: {
    width: '12rem',
  },
  dialogContent: {
    ...shorthands.padding('1rem', '0px'),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dialogField: {
    width: '3rem',
  },
  dialogClose: {
    position: 'absolute',
    top: '0.5rem',
    right: '0.5rem',
  },
  actorsGrid: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateRows: 'repeat(2, 24px)',
    marginLeft: '1rem',
  },
  actorsGridItem: {
    ...shorthands.border('none'),
    ...shorthands.border('1px', 'solid', '#ccc'),
    ...shorthands.borderRadius(0),
    marginRight: '-1px',
    marginBottom: '-1px',
    color: 'red',
    paddingBottom: '0.25rem',
    fontWeight: 'bold',
    '&:hover': {
      color: 'red',
    },
    '&:hover:active': {
      color: 'red',
    },
  },
  activeActorsGridItem: {
    ...shorthands.border('3px', 'solid', '#aaa'),
  },
});

export const ActorsMenu: FC = () => {
  const [activeActor, setActiveActor] = useState<string | null>(null);
  const [newActorName, setNewActorName] = useState<string>('');

  const classes = useStyles();

  return (
    <>
      <div
        className={classes.actorsGrid}
        onDragOver={e =>
          interactivity.interaction.type === InteractionType.POINTER &&
          e.preventDefault()
        }
        onDrop={e => {
          const actor = getDraggedActorName(e);
          if (!actor) return;

          const onGrid = gridState.getActorLocation(actor);
          if (!onGrid) return;

          gridState.clear(onGrid);
        }}
      >
        <Observer>
          {() => {
            const final = [...actors.actors.slice()];
            // We display at least 10 squares, add missing up to 10.
            let missing = Math.max(10 - final.length, 0);
            // If we have more than ten but odd number we need to add one extra to fill empty space in grid.
            if (!missing && final.length % 2 !== 0) missing = 1;
            if (missing) final.push(...new Array(missing).fill(''));

            return (
              <>
                {final.map((name, i) => {
                  const isPlaceholder = !name;
                  const isActive = !isPlaceholder && activeActor === name;

                  return (
                    <Button
                      key={i}
                      appearance="secondary"
                      name="actor"
                      value={name}
                      icon={<>{name}</>}
                      className={mergeClasses(
                        classes.actorsGridItem,
                        isActive ? classes.activeActorsGridItem : undefined,
                      )}
                      size="small"
                      onClick={() => !isPlaceholder && setActiveActor(name)}
                      onDragStart={e => startActorDrag(e, name)}
                      draggable={
                        !isPlaceholder &&
                        !interactivity.paused &&
                        interactivity.interaction.type ===
                          InteractionType.POINTER
                      }
                    />
                  );
                })}
              </>
            );
          }}
        </Observer>
      </div>
      <Toolbar vertical>
        <Dialog
          modalType="non-modal"
          onOpenChange={(_, data) => {
            setNewActorName('');
            interactivity.paused = data.open;
          }}
        >
          <DialogTrigger action="open">
            <ToolbarButton icon={<RxPlusCircled />} />
          </DialogTrigger>
          <DialogSurface
            className={classes.dialog}
            onMouseDown={e => e.stopPropagation()}
          >
            <DialogBody>
              <DialogTitle />
              <DialogContent className={classes.dialogContent}>
                <Label htmlFor="name" size="large">
                  Znak hrdiny:
                </Label>
                <Input
                  id="name"
                  type="text"
                  size="large"
                  inputMode="text"
                  autoComplete="off"
                  value={newActorName}
                  onChange={(_, data) => {
                    if (data.value.length > 1) return;
                    setNewActorName(data.value.toUpperCase());
                  }}
                  className={classes.dialogField}
                />
              </DialogContent>
              <DialogActions>
                <DialogTrigger action="close">
                  <Button
                    appearance="primary"
                    icon={<VscSave />}
                    size="large"
                    onClick={() => {
                      if (!newActorName) return;
                      actors.add(newActorName);
                    }}
                  />
                </DialogTrigger>
              </DialogActions>
            </DialogBody>
          </DialogSurface>
        </Dialog>
        <ToolbarButton
          icon={<RxCrossCircled />}
          onClick={() => {
            if (!activeActor) return;

            actors.delete(activeActor);
            setActiveActor(null);
          }}
        />
      </Toolbar>
    </>
  );
};
