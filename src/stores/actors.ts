import { action, computed, observable } from 'mobx';
import { gridState } from './gridState';
import { z } from 'zod';

export const Actor = z.string().length(1);

export type Actor = z.infer<typeof Actor>;

class Actors {
  @computed get actors(): Actor[] {
    return this._actors;
  }

  constructor() {
    this._actors = [];
  }

  @action add(name: string) {
    name = Actor.parse(name);

    this._actors = [...this.actors, name];
  }

  @action delete(name: string) {
    name = Actor.parse(name);

    const onGrid = gridState.getActorLocation(name);
    if (onGrid) gridState.clear(onGrid);

    this._actors = this._actors.filter(a => a != name);
  }

  @action deleteAll() {
    for (const actor of this._actors) this.delete(actor);
  }

  @observable private accessor _actors: Actor[];
}

export const actors = new Actors();
