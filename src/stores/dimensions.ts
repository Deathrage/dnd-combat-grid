import { action, computed, observable } from 'mobx';

class Dimensions {
  @computed get height() {
    return this._height;
  }

  @computed get width() {
    return this._width;
  }

  constructor() {
    window.addEventListener('resize', () => this.updateDimensions());
    this.updateDimensions();
  }

  @observable private accessor _height: number = 0;

  @observable private accessor _width: number = 0;

  @action private updateDimensions() {
    this._height = window.innerHeight;
    this._width = window.innerWidth;
  }
}

export const dimensions = new Dimensions();
