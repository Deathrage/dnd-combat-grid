import { reaction, runInAction } from 'mobx';
import { actors } from './actors';
import { gridState } from './gridState';
import { GridStateSnapshot } from './gridState/types';
import { z } from 'zod';
import { fromZodError } from 'zod-validation-error';
import { invoke_markChange } from '../BE';

const PersistedState = z.object({
  actors: z.array(z.string().length(1)),
  grid: GridStateSnapshot,
});

type PersistedState = z.infer<typeof PersistedState>;

// Setup reactions when anything that is persisted changes.
reaction(() => actors.actors, invoke_markChange);
reaction(() => gridState.state, invoke_markChange);

export const persistStatesToJson = (): string => {
  const persisted: PersistedState = {
    actors: actors.actors.slice(),
    grid: gridState.state,
  };

  return JSON.stringify(persisted, null, 2);
};

export const rehydrateStateFromJson = (json: string) => {
  const strState = JSON.parse(json);

  let state: PersistedState;
  try {
    state = PersistedState.parse(strState);
  } catch (err) {
    throw (
      'Data is corrupted.\r\n\r\n' +
      fromZodError(err, {
        prefix: null,
        issueSeparator: '.\r\n',
      })
    );
  }

  // Transaction
  runInAction(() => {
    // Deleting actor triggers grid lookup. Even if setting state clears the grid, this saves performance.
    wipeState();

    for (const actor of state.actors) actors.add(actor);

    gridState.state = state.grid;
  });
};

export const wipeState = () => {
  gridState.clear();
  actors.deleteAll();
};
