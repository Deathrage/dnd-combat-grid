import { Coordinates } from './types';

const SERIALIZED_COORDINATES_REGEX = /^x:([-\d.]+)\|y:([-\d.]+)$/;

export const coordinatesEquals = (a: Coordinates, b: Coordinates) =>
  a.x === b.x && a.y === b.y;

export const serializeCoordinates = ({ x, y }: Coordinates): string =>
  `x:${x}|y:${y}`;

export const isSerializedCoordinates = (value: string) =>
  SERIALIZED_COORDINATES_REGEX.test(value);

export const deserializeCoordinates = (coordinates: string) => {
  const match = coordinates.match(SERIALIZED_COORDINATES_REGEX);

  if (match?.length !== 3) throw new Error('Invalid coordinates!');

  return {
    x: Number(match[1]),
    y: Number(match[2]),
  } as Coordinates;
};
