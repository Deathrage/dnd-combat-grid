import { z } from 'zod';
import { isSerializedCoordinates } from './helpers';
import { Actor } from '../actors';

export interface Coordinates {
  x: number;
  y: number;
}

export const ActorItem = z.object({
  type: z.literal('actor'),
  name: Actor,
});

export type ActorItem = z.infer<typeof ActorItem>;

export const ColorItem = z.object({
  type: z.literal('color'),
  color: z.string(),
});

export type ColorItem = z.infer<typeof ColorItem>;

export const Item = z.union([ActorItem, ColorItem]);

export type Item = z.infer<typeof Item>;

export const GridStateSnapshot = z.record(
  z.string().refine(
    value => isSerializedCoordinates(value),
    value => ({ message: `Coordinates "${value}" are not valid.` }),
  ),
  Item,
);

export type GridStateSnapshot = Record<string, Item>;
