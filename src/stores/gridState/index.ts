import { action, computed, observable } from 'mobx';
import { Coordinates, GridStateSnapshot, Item } from './types';
import {
  coordinatesEquals,
  deserializeCoordinates,
  serializeCoordinates,
} from './helpers';

class GridState {
  @computed get state(): GridStateSnapshot {
    const state = Object.fromEntries(this.#all.entries());
    return state;
  }

  set state(state: GridStateSnapshot) {
    this.#clearAll();

    for (const [strLocation, item] of Object.entries(state)) {
      const location = deserializeCoordinates(strLocation);
      this.#addItem(location, item);
    }
  }

  constructor() {
    this.#all = observable.map({}, { deep: false });
    this.#actors = new Map();
  }

  get(location: Coordinates) {
    return this.#all.get(serializeCoordinates(location));
  }

  getActorLocation(name: string) {
    return this.#actors.get(name);
  }

  @action touch(location: Coordinates, item: Item) {
    if (item.type === 'actor') {
      const alreadyOnGrid = this.#actors.get(item.name);

      if (alreadyOnGrid && coordinatesEquals(alreadyOnGrid, location)) return;

      if (alreadyOnGrid) this.clear(alreadyOnGrid);
    }

    this.#addItem(location, item);
  }

  @action clear(location?: Coordinates) {
    if (!location) {
      this.#clearAll();
      return;
    }

    const strLocation = serializeCoordinates(location);

    const existing = this.#all.get(strLocation);

    if (existing) this.#all.delete(strLocation);
    if (existing?.type === 'actor') this.#actors.delete(existing.name);
  }

  #all: Map<string, Item>;
  // Tracks where are individual actors to prevent dupes.
  #actors: Map<string, Coordinates>;

  #addItem(location: Coordinates, item: Item) {
    this.#all.set(serializeCoordinates(location), item);
    if (item.type === 'actor') this.#actors.set(item.name, location);
  }

  #clearAll() {
    this.#all.clear();
    this.#actors.clear();
  }
}

export const gridState = new GridState();
