import { action, computed, observable } from 'mobx';

// W3C const for eraser
const ERASER_BUTTON = 5;

export enum InteractionType {
  BRUSH = 'BRUSH',
  ERASER = 'ERASER',
  POINTER = 'POINTER',
}

export type Interaction =
  | {
      type: InteractionType.POINTER;
    }
  | {
      type: InteractionType.BRUSH;
      color: string;
      active: boolean;
      treatAsEraser: boolean;
    }
  | {
      type: InteractionType.ERASER;
      active: boolean;
    };

class Interactivity {
  @observable accessor paused: boolean = false;

  @computed get interaction() {
    return this._interaction;
  }

  constructor() {
    window.addEventListener(
      'pointerdown',
      action((e: PointerEvent) => {
        const element = e.target as Element;
        element.releasePointerCapture(e.pointerId);

        if (this._interaction.type === InteractionType.BRUSH) {
          this._interaction = {
            ...this._interaction,
            active: true,
            treatAsEraser: e.button === ERASER_BUTTON,
          };
        }
        if (this._interaction.type === InteractionType.ERASER) {
          this._interaction = {
            ...this._interaction,
            active: true,
          };
        }
      }),
    );

    window.addEventListener(
      'pointerup',
      action(() => {
        if (this._interaction.type === InteractionType.BRUSH) {
          this._interaction = {
            ...this._interaction,
            active: false,
            treatAsEraser: false,
          };
        }
        if (this._interaction.type === InteractionType.ERASER) {
          this._interaction = {
            ...this._interaction,
            active: false,
          };
        }
      }),
    );
  }

  @action switch(interaction: Interaction) {
    this._interaction = interaction;
  }

  switchToBrush(color: string) {
    this.switch({
      type: InteractionType.BRUSH,
      color,
      active: false,
      treatAsEraser: false,
    });
  }

  switchToEraser() {
    this.switch({
      type: InteractionType.ERASER,
      active: false,
    });
  }

  switchToPointer() {
    this.switch({
      type: InteractionType.POINTER,
    });
  }

  @observable private accessor _interaction: Interaction = {
    type: InteractionType.POINTER,
  };
}

export const interactivity = new Interactivity();
