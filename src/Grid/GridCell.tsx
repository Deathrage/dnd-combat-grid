import { FC, PointerEventHandler, useCallback } from 'react';
import { Coordinates } from '../stores/gridState/types';
import { Observer } from 'mobx-react-lite';
import {
  makeStyles,
  mergeClasses,
  shorthands,
  tokens,
} from '@fluentui/react-components';
import { gridState } from '../stores/gridState';
import { InteractionType, interactivity } from '../stores/interactivity';
import { startActorDrag, getDraggedActorName } from './drag';

export const CELL_SIZE_PX = 24;

const useStyles = makeStyles({
  td: {
    width: `${CELL_SIZE_PX}px`,
    height: `${CELL_SIZE_PX}px`,
    boxSizing: 'border-box',
    textAlign: 'center',
    userSelect: 'none',
    fontSize: '20px',
    fontFamily: tokens.fontFamilyBase,
    ...shorthands.padding(0),
    ...shorthands.margin(0),
    ...shorthands.border('1px', 'solid', '#ccc'),
  },
  actorTd: {
    color: 'red',
    fontWeight: 'bold',
    '[draggable]': {
      cursor: 'grab',
      ':active': {
        cursor: 'grabbing',
      },
    },
  },
});

window.addEventListener('pointercancel', () => console.log('cancel'));

export const GridCell: FC<{ location: Coordinates }> = ({ location }) => {
  const classes = useStyles();

  const onPointerMove = useCallback<
    PointerEventHandler<HTMLTableCellElement>
  >(() => {
    if (interactivity.paused) return;
    if (
      interactivity.interaction.type !== InteractionType.BRUSH &&
      interactivity.interaction.type !== InteractionType.ERASER
    )
      return;
    if (!interactivity.interaction.active) return;

    switch (interactivity.interaction.type) {
      case InteractionType.BRUSH:
        if (!interactivity.interaction.treatAsEraser) {
          gridState.touch(location, {
            type: 'color',
            color: interactivity.interaction.color,
          });
          break;
        }
      /* FALLTHROUGH if brush was attempted using stylus eraser*/
      case InteractionType.ERASER:
        gridState.clear(location);
        break;
    }
  }, [location]);

  return (
    <Observer>
      {() => {
        const maybeItem = gridState.get(location);

        if (maybeItem?.type === 'actor')
          return (
            <td
              className={mergeClasses(classes.td, classes.actorTd)}
              onDragStart={e => startActorDrag(e, maybeItem.name)}
              draggable={
                !interactivity.paused &&
                interactivity.interaction.type === InteractionType.POINTER
              }
            >
              {maybeItem.name}
            </td>
          );
        if (maybeItem?.type === 'color')
          return (
            <td
              className={classes.td}
              style={{ backgroundColor: maybeItem.color }}
              onPointerMove={onPointerMove}
            />
          );

        return (
          <td
            className={classes.td}
            onPointerMove={onPointerMove}
            onDragOver={e =>
              interactivity.interaction.type === InteractionType.POINTER &&
              e.preventDefault()
            }
            onDrop={e => {
              const name = getDraggedActorName(e);
              if (!name) return;

              gridState.touch(location, { type: 'actor', name });
            }}
          />
        );
      }}
    </Observer>
  );
};
