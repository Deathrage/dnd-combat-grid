import { makeStyles, mergeClasses } from '@fluentui/react-components';
import { Observer } from 'mobx-react-lite';
import { FC } from 'react';
import { dimensions } from '../stores/dimensions';
import { interactivity } from '../stores/interactivity';
import { CursorClassName, getCursorClassName } from './helpers';
import { CELL_SIZE_PX } from './GridCell';
import { GridBody } from './GridBody';

const useStyles = makeStyles({
  table: { borderCollapse: 'collapse', tableLayout: 'fixed' },
  [CursorClassName.BRUSH]: {
    cursor: `url(/rxPencil.png) 0 23, crosshair`,
  },
  [CursorClassName.ERASER]: {
    cursor: `url(/rxEraser.png) 7 15, crosshair`,
  },
});

export const Grid: FC = () => {
  const classes = useStyles();

  return (
    <Observer>
      {() => {
        const colCount = Math.ceil(dimensions.width / CELL_SIZE_PX);
        const rowCount = Math.ceil(dimensions.height / CELL_SIZE_PX);

        return (
          <table
            className={mergeClasses(
              classes.table,
              classes[
                getCursorClassName(
                  interactivity.interaction.type,
                  interactivity.paused,
                )!
              ],
            )}
          >
            <GridBody rowCount={rowCount} colCount={colCount} />
          </table>
        );
      }}
    </Observer>
  );
};
