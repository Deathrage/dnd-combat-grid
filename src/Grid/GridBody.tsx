import { ReactElement, memo } from 'react';
import { GridCell } from './GridCell';

export const GridBody = memo(
  ({ rowCount, colCount }: { rowCount: number; colCount: number }) => {
    const rows: ReactElement[] = [];

    for (let y = 0; y <= rowCount; y++) {
      const cols: ReactElement[] = [];

      for (let x = 0; x <= colCount; x++)
        cols.push(<GridCell key={x} location={{ x, y }} />);

      rows.push(<tr key={y}>{cols}</tr>);
    }

    return <tbody>{rows}</tbody>;
  },
);
