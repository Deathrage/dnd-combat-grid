import { DragEvent } from 'react';

export const startActorDrag = (e: DragEvent, name: string) => {
  e.dataTransfer.setData('actor', name);
  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.dropEffect = 'move';
};

export const getDraggedActorName = (e: DragEvent) => {
  const name = e.dataTransfer.getData('actor');
  if (!name) return null;
  return name;
};
