import { InteractionType } from '../stores/interactivity';

export enum CursorClassName {
  BRUSH = 'cursorBrush',
  ERASER = 'cursorEraser',
}

const cursors: Record<InteractionType, CursorClassName | null> = {
  [InteractionType.BRUSH]: CursorClassName.BRUSH,
  [InteractionType.ERASER]: CursorClassName.ERASER,
  [InteractionType.POINTER]: null,
};

export const getCursorClassName = (
  interactionType: InteractionType,
  paused: boolean,
) => {
  if (paused) return null;
  return cursors[interactionType];
};
