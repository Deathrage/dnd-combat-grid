import {
  Button,
  Dialog,
  DialogActions,
  DialogBody,
  DialogContent,
  DialogSurface,
  DialogTitle,
  DialogTrigger,
} from '@fluentui/react-components';
import { Component, PropsWithChildren } from 'react';

export class ErrorBoundary extends Component<
  PropsWithChildren<{}>,
  { error: string | null; open: boolean }
> {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      open: false,
    };
  }

  componentDidMount() {
    addEventListener('unhandledrejection', event =>
      this.setState({ error: String(event.reason), open: true }),
    );
  }

  static getDerivedStateFromError(error: unknown) {
    return { error: String(error), open: true };
  }

  render() {
    return (
      <>
        {this.props.children}
        <Dialog
          modalType="non-modal"
          open={this.state.open}
          onOpenChange={(_, { open }) => this.setState({ open })}
        >
          <DialogSurface>
            <DialogBody>
              <DialogTitle>Error</DialogTitle>
              <DialogContent style={{ whiteSpace: 'pre-wrap' }}>
                {this.state.error}
              </DialogContent>
              <DialogActions>
                <DialogTrigger>
                  <Button appearance="primary">Close</Button>
                </DialogTrigger>
              </DialogActions>
            </DialogBody>
          </DialogSurface>
        </Dialog>
      </>
    );
  }
}
