import React from 'react';
import ReactDOM from 'react-dom/client';
import { Grid } from './Grid';
import {
  FluentProvider,
  webLightTheme,
  makeStaticStyles,
} from '@fluentui/react-components';
import { Menu } from './Menu';
import { ErrorBoundary } from './ErrorBoundary';

const useStaticStyles = makeStaticStyles({
  body: {
    margin: 0,
    overflow: 'hidden',
    touchAction: 'none',
  },
});

const App = () => {
  useStaticStyles();

  return (
    <FluentProvider theme={webLightTheme}>
      <ErrorBoundary>
        <Menu />
        <Grid />
      </ErrorBoundary>
    </FluentProvider>
  );
};

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
