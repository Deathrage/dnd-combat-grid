import { invoke } from '@tauri-apps/api';

export const invoke_new = () => invoke('new');

export const invoke_open = () => invoke<string>('open');

export const invoke_save = (data: string) => invoke('save', { data });

export const invoke_saveAs = (data: string) => invoke('save_as', { data });

export const invoke_markChange = () => invoke('mark_change');
