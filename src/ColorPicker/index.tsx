import { IColorCellProps, SwatchColorPicker } from '@fluentui/react';
import { FC } from 'react';
import { swatches } from './swatches';
import { Observer } from 'mobx-react-lite';
import { InteractionType, interactivity } from '../stores/interactivity';

const cells: IColorCellProps[] = swatches.map(swatch => ({
  id: swatch.color,
  ...swatch,
}));

export const ColorPicker: FC = () => (
  <Observer>
    {() => (
      <SwatchColorPicker
        columnCount={10}
        doNotContainWithinFocusZone
        cellShape={'square'}
        colorCells={cells}
        // TODO: Null to unselect yet types do not allow it
        selectedId={
          interactivity.interaction.type === InteractionType.BRUSH
            ? interactivity.interaction.color
            : null!
        }
        onChange={(_, color) => color && interactivity.switchToBrush(color)}
      />
    )}
  </Observer>
);
