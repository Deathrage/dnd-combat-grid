export interface Swatch {
  color: string;
  label: string;
}

export const swatches: Swatch[] = [
  { label: 'black', color: '#000000' },
  { label: 'grey', color: '#808080' },
  { label: 'dark-brown', color: '#800000' },
  { label: 'red', color: '#ff0000' },
  { label: 'dark-orange', color: '#ffc90e' },
  { label: 'yellow', color: '#ffff00' },
  { label: 'green', color: '#008000' },
  { label: 'blue', color: '#0080ff' },
  { label: 'dark-blue', color: '#0000a0' },
  { label: 'violet', color: '#800080' },

  { label: 'white', color: '#ffffff' },
  { label: 'light-grey', color: '#c0c0c0' },
  { label: 'brown', color: '#C07070' },
  { label: 'pink', color: '#ff80c0' },
  { label: 'orange', color: '#ff8000' },
  { label: 'pale-yellow', color: '#ffff80' },
  { label: 'lime', color: '#80ff00' },
  { label: 'light-blue', color: '#80ffff' },
  { label: 'blue-grey', color: '#7092be' },
  { label: 'salmon', color: '#ff8080' },
];
